import javafx.application.Application;
import javafx.stage.*;
import javafx.scene.layout.*;
import javafx.geometry.*;
import javafx.scene.shape.*;
import javafx.scene.paint.Color;
import javafx.scene.text.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import java.util.ArrayList;

public class Avl extends Application {

	static Integer nodes = 0;
	static Integer height = 0;
	static Text num_nodes;
	static Text tree_height;
	static StackPane avl;
	static ArrayList<Node> in_avl;
	static TextField in;
	static TextField del;
	static Node root;
	static Pane node_height;
	static Pane balencebox;
	private static boolean continueAvl = true;
	private static Rectangle r3;

	public static void main(String[] args) {
		launch(args);
	}

	public void start(Stage stage) {

		BorderPane top = new BorderPane();
		top.setPadding(new Insets(30));

		// Set up v box to store info in

		Pane info = new VBox(10);

		// set up the add and delete portion
		Pane in_del = new HBox(10);
		Label l1 = new Label("Insert");
		in = new TextField();
		Label l2 = new Label("Delete");
		del = new TextField();

		// add all this to the in_del h box
		in_del.getChildren().addAll(l1, in, l2, del);

		// Add create a pane to hold the number of nodes and height of the tree
		node_height = new HBox(25);

		Text t4 = new Text("Number of Nodes: ");
		Text t5 = new Text("Tree Height: ");

		num_nodes = new Text(nodes.toString());
		tree_height = new Text(height.toString());
		node_height.getChildren().addAll(t4, num_nodes, t5, tree_height);

		Text t3 = new Text("AVL Tree Editor");
		info.getChildren().add(t3);
		info.getChildren().add(in_del);
		info.getChildren().add(node_height);

		avl = new StackPane();
		r3 = new Rectangle(0, 0, 1400, 600);
		r3.setFill(Color.TRANSPARENT);
		r3.setStroke(Color.BLACK);
		avl.getChildren().add(r3);

		top.setTop(info);
		top.setCenter(avl);

		// Handel when the enter key is pressed after typing in insert box
		in.setOnKeyPressed(e -> InKeyPressed(e));
		del.setOnKeyPressed(e -> DelKeyPressed(e));

		Scene scene = new Scene(top);

		stage.setScene(scene);

		stage.show();
	}

	/**
	 * Method that will determine when the key pressed is enter
	 * 
	 * @param evt
	 */
	private void InKeyPressed(javafx.scene.input.KeyEvent evt) {
		if (evt.getCode().equals(KeyCode.ENTER)) {
			begin_add(continueAvl);
		}
	}

	/**
	 * Method that sets the action on the delete box typed in
	 * @param evt
	 */
	private void DelKeyPressed(javafx.scene.input.KeyEvent evt) {
		if (evt.getCode().equals(KeyCode.ENTER)) {
			begin_rem();
		}
	}

	/**
	 * Method that will begin the add function by reading in what the user typed
	 * @param b
	 */
	private static void begin_add(boolean b) {

		if (b) {
			String s = in.getText();
			try {
				int value = Integer.parseInt(s);
				in.clear();

				if (nodes == 0) {
					avl.getChildren().add(add_first(value));
				} else

					// see where the node should be added
					addWhere(value, root);
				
			} catch (NumberFormatException e) {
				in.clear();
				return;
			}

		} else {
			in.clear();
		}
		height = Treeheight(root);
		updateInfo();
	}
	
	/**
	 * Method that will update the trees height 
	 * @param cur
	 * @return
	 */
	private static int Treeheight(Node cur) {
		//if the current node is null return 0
		if(cur == null) {
			return 0;
		}
		return maximum(1+ Treeheight(cur.getRight_Child()) , 1+ Treeheight(cur.getLeft_Child()));
	}

	/**
	 * Method that will seach through the tree and find where the node needs to
	 * be added
	 * 
	 * @param val
	 *            the value of the node that is added
	 * @param n
	 *            the parent node (so initially the root of the tree)
	 */
	private static void addWhere(int val, Node n) {

		// base case for less than
		if (val < n.getValue() && n.getLeft_Child() == null) {
			// add it to the left child

			// find the new x value
			Node n1 = new Node(null, null, n, val, n.getX() - (250 / NodeHeight(n)), n.getY() + 50);
			// Check to see if the avl tree is broken
			n1.setHeight(NodeHeight(n1));
			n.setLeft(n1);
			addNode_left(n1);

			return;

		}

		// base case for greater than
		else if (val > n.getValue() && n.getRight_Child() == null) {
			Node n1 = new Node(null, null, n, val, n.getX() + (250 / NodeHeight(n)), n.getY() + 50);
			// Check to see if the avl tree is broken
			n1.setHeight(NodeHeight(n1));
			n.setRight(n1);
			addNode_right(n1);
			return;
		}

		// else search left
		else if (val < n.getValue() && n.getLeft_Child() != null) {
			addWhere(val, n.getLeft_Child());
		}

		// else search right
		else if (val > n.getValue() && n.getRight_Child() != null) {
			addWhere(val, n.getRight_Child());
		} else
			return;

	}

	/**
	 * Method that will begin the remove node function by reading in what the user typed
	 */
	private static void begin_rem() {

		int value;
		String s = del.getText();
		try {
			value = Integer.parseInt(s);
			del.clear();
			
		} catch (NumberFormatException e) {
			del.clear();
			return;
		}
		if (root != null) {
			Node remove = removeWhere(value, root);

			if (remove != null) {

		
				removeNode(remove);
			}

			if (root != null) {
				findLeaves(root);
			}
		}
		height = Treeheight(root);
		updateInfo();

	}

	/** 
	 * Method that will determine where we need to remove the node 
	 * 
	 * @param i value of the node we are removing 
	 * @param n node that we are currently checking 
	 * @return the node that needs to be removed 
	 */
	private static Node removeWhere(int i, Node n) {

		// check to see if you are at the node
		if (n.getValue() == i) {
			return n;
		}
		if (i > n.getValue() && n.getRight_Child() != null) {
			return removeWhere(i, n.getRight_Child());
		}
		if (i < n.getValue() && n.getLeft_Child() != null) {
			return removeWhere(i, n.getLeft_Child());
		} else
			return null;
	}

	/**
	 * Method that will remove a Node n
	 * 
	 * @param n
	 */
	private static void removeNode(Node n) {

		// check to see if it is the root are any children
		if (n.getParentNode() == null) {
			// check to see if there are any children
			if (n.getLeft_Child() == null && n.getRight_Child() == null) {
				root = null;

				avl.getChildren().remove(n);
				n = null;
			} else if (n.getRight_Child() != null && n.getLeft_Child() == null) {

				n.getRight_Child().setParentNode(null);
				root = n.getRight_Child();

				// recursiveBalance(n.getRight_Child());
				n = null;
			}

			else if (n.getRight_Child() == null && n.getLeft_Child() != null) {
				n.getLeft_Child().setParentNode(n);
				root = n.getLeft_Child();

				// recursiveBalance(n.getLeft_Child());
				n = null;
			}

			// else there are two children set the left child to be the new root
			// and add the right children to the right most child of the new
			// root
			else {
				Node left = n.getLeft_Child();
				Node right = n.getRight_Child();
				left.setParentNode(null);
				Node cur = left;
				while (cur.getRight_Child() != null) {
					cur = cur.getRight_Child();
				}
				cur.setRight(right);
				right.setParentNode(cur);
				root = left;
				n = null;

				// now you need to balance from the right most child
				cur = root;
				while (cur.getRight_Child() != null) {
					cur = cur.getRight_Child();
				}
				recursiveBalance(cur);
			}

		}

		// There is only one child of the node being removed
		else if (n.getRight_Child() == null && n.getLeft_Child() != null) {


			if (n.getValue() > n.getParentNode().getValue()) {
				n.getParentNode().setRight(n.getLeft_Child());
				n.getLeft_Child().setParentNode(n.getParentNode());
				// recursiveBalance(n.getLeft_Child());
				n = null;
			} else {
				n.getParentNode().setLeft(n.getLeft_Child());
				n.getLeft_Child().setParentNode(n.getParentNode());
				// recursiveBalance(n.getLeft_Child());
				n = null;
			}
		}

		else if (n.getRight_Child() != null && n.getLeft_Child() == null) {

			if (n.getValue() > n.getParentNode().getValue()) {
				n.getParentNode().setRight(n.getRight_Child());
				n.getRight_Child().setParentNode(n.getParentNode());

				// recursiveBalance(n.getRight_Child());
				n = null;
			} else {
				n.getParentNode().setLeft(n.getRight_Child());
				n.getRight_Child().setParentNode(n.getParentNode());

				// recursiveBalance(n.getRight_Child());
				n = null;
			}

		}

		// there are no kids so just set the parent's nodes child to null
		else if (n.getRight_Child() == null && n.getLeft_Child() == null) {

			if (n.getValue() < n.getParentNode().getValue()) {
				n.getParentNode().setLeft(null);
			} else {
				n.getParentNode().setRight(null);
			}

			// recursiveBalance(n.getParentNode());

			avl.getChildren().remove(n);
			n = null;
		}

		// Else there are multiple children of the Node
		else {

			Node p = n.getParentNode();

			Node right = n.getRight_Child();

			Node left = n.getLeft_Child();

			// the node that was removed was the left child
			if (p.getValue() > n.getValue()) {

				p.setLeft(right);

				right.setParentNode(p);

				// find the left most child of n's right child
				Node curr = right;

				while (curr.getLeft_Child() != null) {
					curr = curr.getLeft_Child();
				}

				left.setParentNode(curr);
				curr.setLeft(left);

			}

			// the node that was removed was the right child
			else {

				p.setRight(right);

				right.setParentNode(p);

				// find the left most child of n's right child
				Node curr = right;

				while (curr.getLeft_Child() != null) {
					curr = curr.getLeft_Child();
				}

				left.setParentNode(curr);
				curr.setLeft(left);

			}

			avl.getChildren().remove(n);
			n = null;

		}

		nodes--;

		height = Treeheight(root);
		updateInfo();

	}

	/**
	 * Method that finds the leaves of the tree so they can all be recursivly balanced 
	 * @param n
	 */
	private static void findLeaves(Node n) {

		if (n.getRight_Child() == null && n.getLeft_Child() == null) {
			// you have found a leave so do the recursive balance on it
			recursiveBalance(n);
		}
		if (n.getRight_Child() != null) {
			findLeaves(n.getRight_Child());
		}

		if (n.getLeft_Child() != null) {
			findLeaves(n.getLeft_Child());
		}

	}

	/**
	 * Method that will return true if the tree needs to be balanced
	 * 
	 * @param n
	 *            node that is being added
	 * @return true if needs to be balanced, false if it does not
	 */

	private static void setBalance(Node cur) {

		cur.setB(height(cur.getRight_Child()) - height(cur.getLeft_Child()));
	}

	/**
	 * Method that adds the first node of the tree so this will be the root node
	 * 
	 * @param new_int
	 *            the value of the first node that is being added
	 * @return
	 */
	private static Node add_first(int new_int) {

		Node first = new Node(null, null, null, new_int, 700, 25);
		height = 1;
		first.setHeight(1);
		tree_height = new Text("" + height);
		root = first;
		// update the number of nodes
		nodes++;
		num_nodes = new Text("" + nodes);
		return first;

	}

	/**
	 * Method that is adding a new node to the left of a current node if the
	 * current node if it is not breaking the avl property --> if it does break
	 * property a box will come up to allow a shift to occur
	 * 
	 * @param n
	 */
	private static void addNode_left(Node n) {

		avl.getChildren().add(n);
		n.updateHeight();
		updatedB(n);
		if (getBalance(n)) {
			balenceBox(n, "left");
		}

		if (NodeHeight(n) > height) {
			height = NodeHeight(n);
		}
		// update the number of nodes
		nodes++;
		num_nodes = new Text("" + nodes);
	}

	/**
	 * Method that will add a new node to the right of a current node this will
	 * happen if the node is not breaking the avl tree properties
	 * 
	 * @param n
	 *            node that is being added
	 */
	private static void addNode_right(Node n) {
		n.updateHeight();
		avl.getChildren().add(n);
		updatedB(n);
		if (getBalance(n)) {
			balenceBox(n, "right");
		}
		if (NodeHeight(n) > height) {
			height = NodeHeight(n);
		}
		// update the number of nodes
		nodes++;
		num_nodes = new Text("" + nodes);
	}

	/**
	 * Method that updates the balance of the node 
	 * @param n
	 */
	private static void updatedB(Node n) {
		setBalance(n);
		if (n.getParentNode() != null) {
			updatedB(n.getParentNode());
		}
	}

	/**
	 * Method that returns true if we need to balance the tree
	 * @param n
	 * @return
	 */
	private static boolean getBalance(Node n) {

		if (Math.abs(n.getBalance()) >= 2) {
			return true;
		} else if (n.getParentNode() != null) {
			return getBalance(n.getParentNode());
		} else {
			return false;
		}
	}

	/**
	 * Method to update the node and height info
	 */
	public static void updateInfo() {
		node_height.getChildren().clear();
		Text t4 = new Text("Number of Nodes: ");
		Text t5 = new Text("Tree Height: ");
		num_nodes = new Text(nodes.toString());
		tree_height = new Text(height.toString());
		node_height.getChildren().addAll(t4, num_nodes, t5, tree_height);
	}

	/**
	 * Method that will pull up a request to balance if the avl property is broken 
	 * @param n
	 * @param s
	 */
	private static void balenceBox(Node n, String s) {

		continueAvl = false;
		balencebox = new VBox();
		Text t = new Text("Avl Property violated, will balence");
		Pane buttons = new HBox(20);
		Button buOk = new Button("Okay");
		Button buCan = new Button("Cancel");
		buttons.getChildren().addAll(buOk, buCan);
		balencebox.getChildren().addAll(t, buttons);
		avl.getChildren().add(balencebox);
		buOk.setOnMouseClicked(e -> recursiveBalance(n));
		buCan.setOnMouseClicked(e -> dontAdd(n, s));

	}

	/**
	 * Method that does a left single rotaion 
	 * @param n
	 * @return
	 */
	private static Node leftSingle(Node n) {
		System.out.println("left single");
		Node v = n.getRight_Child();
		v.setParentNode(n.getParentNode());

		n.setRight(v.getLeft_Child());

		if (n.getRight_Child() != null) {
			n.getRight_Child().setParentNode(n);
		}

		v.setLeft(n);
		n.setParentNode(v);

		if (v.getParentNode() != null) {
			if (v.getParentNode().getRight_Child() == n) {
				v.getParentNode().setRight(v);
			} else if (v.getParentNode().getLeft_Child().equals(n)) {
				v.getParentNode().setLeft(v);
			}
		}

		setBalance(n);
		setBalance(v);
		return v;
	}

	/**
	 * Method that does a right single rotation 
	 * @param n
	 * @return
	 */
	private static Node rightSingle(Node n) {
		System.out.println("right single");
		Node v = n.getLeft_Child();
		v.setParentNode(n.getParentNode());

		n.setLeft(v.getRight_Child());

		if (n.getLeft_Child() != null) {
			n.getLeft_Child().setParentNode(n);
		}

		v.setRight(n);
		n.setParentNode(v);

		if (v.getParentNode() != null) {
			if (v.getParentNode().getRight_Child().equals(n)) {
				v.getParentNode().setRight(v);
			} else if (v.getParentNode().getLeft_Child().equals(n)) {
				v.getParentNode().setLeft(v);
			}
		}

		setBalance(n);
		setBalance(v);
		return v;
	}

	/**
	 * Method that does a left right double rotation 
	 * @param n
	 * @return
	 */
	private static Node leftRightDouble(Node n) {
		System.out.println("leftRightDouble");
		n.setRight(rightSingle(n.getRight_Child()));
		return leftSingle(n);

	}

	/**
	 * Method that does a right left double rotation 
	 * @param n
	 * @return
	 */
	private static Node rightLeftDouble(Node n) {
		System.out.println("rightLeftDouble");
		n.setLeft(leftSingle(n.getLeft_Child()));
		return rightSingle(n);

	}

	/**
	 * Method that will allow us to remove the most recently added node if
	 * a balance was required and we don't want to balance
	 * @param n
	 * @param s
	 */
	private static void dontAdd(Node n, String s) {
		Node parent = n.getParentNode();
		if (s.equals("left")) {
			parent.setLeft(null);
		} else
			parent.setRight(null);
		parent.updateHeight();
		continueAvl = true;
		avl.getChildren().remove(n);
		avl.getChildren().remove(balencebox);
		return;

	}

	/**
	 * Method that will find the height of the current node
	 * 
	 * @param node
	 *            who's height we are finding
	 * @return the height
	 */
	private static int NodeHeight(Node n) {
		int count = 1;
		while (n.getParentNode() != null) {
			count++;
			n = n.getParentNode();
		}
		return count;

	}

	/**
	 * Method that recursively balances the tree
	 * @param cur
	 */
	private static void recursiveBalance(Node cur) {

		setBalance(cur);

		cur.updateHeight();
		int bal = cur.getBalance();

		// check the balance
		if (bal <= -2) {

			if (height(cur.getLeft_Child().getLeft_Child()) >= height(cur.getLeft_Child().getRight_Child())) {
				cur = rightSingle(cur);
			} else {
				cur = rightLeftDouble(cur);
			}
		} else if (bal >= 2) {
			if (height(cur.getRight_Child().getRight_Child()) >= height(cur.getRight_Child().getLeft_Child())) {
				cur = leftSingle(cur);
			} else {
				cur = leftRightDouble(cur);
			}
		}

		// we did not reach the root yet
		if (cur.getParentNode() != null) {

			recursiveBalance(cur.getParentNode());
		} else {
			avl.getChildren().clear();
			avl.getChildren().add(r3);
			findNewRoot(cur);
			throughTree(root);
			height = Treeheight(root);
			updateInfo();
			continueAvl = true;

		}
	}

	/**
	 * Node that will update the root of the tree
	 * @param Node n
	 */
	private static void findNewRoot(Node n) {
		if (n.getParentNode() == null) {
			root = n;
		} else {
			findNewRoot(n.getParentNode());
		}
	}

	/**
	 * Method that finds the heights of the nodes by comparing the left and
	 * right children heights and taking the max
	 * 
	 * @param cur
	 *            Node
	 * @return int height
	 */
	private static int height(Node cur) {
		if (cur == null) {
			return -1;
		}
		if (cur.getLeft_Child() == null && cur.getRight_Child() == null) {
			return 0;
		} else if (cur.getLeft_Child() == null) {
			return 1 + height(cur.getRight_Child());
		} else if (cur.getRight_Child() == null) {
			return 1 + height(cur.getLeft_Child());
		} else {
			return 1 + maximum(height(cur.getLeft_Child()), height(cur.getRight_Child()));
		}
	}

	/**
	 * Method that will find the maximum of the two heights, used to find the
	 * height of the node
	 * 
	 * @param height2
	 * @param height1
	 * @return
	 */
	private static int maximum(int height2, int height1) {
		if (height2 > height1) {
			return height2;
		} else
			return height1;
	}

	/**
	 * Method that loops through every node so they can be re added 
	 * @param Node that will start (which should be the root)
	 */
	private static void throughTree(Node n) {
		reAdd(n);
		if (n.getRight_Child() != null) {

			throughTree(n.getRight_Child());
		}

		if (n.getLeft_Child() != null) {
			throughTree(n.getLeft_Child());
		}

	}

	/**
	 * Method that re adds all the nodes to the tree with their updated posistions 
	 * @param Node being added n
	 */
	private static void reAdd(Node n) {
		n.setHeight(NodeHeight(n));
		n.removeConnector();
		n.setX();
		n.setY();
		n.updateConnectors();
		avl.getChildren().add(n);
	}

}
