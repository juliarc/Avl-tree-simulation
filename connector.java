
import javafx.scene.layout.Pane;

import javafx.scene.shape.*;

public class connector extends Pane{
	
	private int startX;
	private int endX;
	private int startY;
	private int endY;
	
	public connector(Node n1, Node n2) { 
		startX = n1.getX();
		endX = n2.getX();
		startY = n1.getY() +15;
		endY = n2.getY() - 15;
		
		Line l = new Line(); 
		l.setStartX(startX);
		l.setEndX(endX);
		l.setStartY(startY);
		l.setEndY(endY);
		
		getChildren().add(l);
	}

}
