
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;

/**
 * Class Nod which extends Pane and implaments the comparable interface This is
 * to store each of the values that are in the pane
 * 
 * @author stephencassella
 *
 */
public class Node extends Pane implements Comparable<Node> {
	private Node right_child;
	private Node left_child;
	private Node parent_node;
	private final int radius = 15;
	private int xVal;
	private int yVal;
	private int height;
	private int balence = 0;
	private int leftHeight;
	private int rightHeight;
	private Text t;
	private Circle c;
	private connector con;
	private int value;

	public Node(Node r, Node l, Node p, int v, int x, int y) {
		right_child = r;
		left_child = l;
		parent_node = p;
		value = v;
		xVal = x;
		yVal = y;

		// set up the heights for each
		if (right_child == null)
			rightHeight = 0;
		else {
			if (right_child.rightHeight > right_child.leftHeight)
				rightHeight = 1 + right_child.rightHeight;
			else
				right_child.rightHeight = 1 + right_child.leftHeight;
		}
		if (left_child == null)
			leftHeight = 0;
		else {
			if (left_child.rightHeight > left_child.leftHeight)
				leftHeight = 1 + left_child.rightHeight;
			else
				left_child.leftHeight = 1 + right_child.leftHeight;
		}

		// create a new circle and text and make that the pane
		c = new Circle(x, y, radius);
		c.setFill(Color.TRANSPARENT);
		c.setStroke(Color.BLACK);
		String val = "" + value;
		t = new Text(val);
		t.setX(x - 7);
		t.setY(y + 5);

		// add connector
		if (parent_node != null) {
			con = new connector(parent_node, this);
			getChildren().addAll(c, t, con);
		} else {
			getChildren().addAll(c, t);
		}

	}

	public void setB(int n) {
		balence = n;
	}

	public int getBalance() {
		return balence;
	}

	public void setHeight(int h) {
		height = h;
	}

	public int getNodeHeight() {
		return height;
	}

	public int getValue() {
		return value;
	}

	public Circle getCircle() {
		return c;
	}

	public int compareTo(Node n) {
		if (value < n.getValue())
			return -1;
		else if (value == n.getValue())
			return 0;
		else
			return 1;
	}

	public Node getLeft_Child() {
		return left_child;

	}

	public Node getRight_Child() {
		return right_child;
	}

	public Node getParentNode() {
		return parent_node;
	}

	public int getX() {
		return xVal;
	}

	public int getY() {
		return yVal;
	}

	public void setX() {
		if (parent_node == null) {
			xVal = 700;
		} else {
			Node p = parent_node;
			if (this == p.getLeft_Child()) {
				xVal = p.xVal - (250 / p.height );
			} else {
				xVal = p.xVal + (250 / p.height );

			}
		}
		c.setCenterX(xVal);
		setValue(value); 
	}

	public void setY() {
		if (parent_node == null) {
			yVal = 25;

		} else {
			yVal = parent_node.yVal + 50;

		}
		c.setCenterY(yVal);
		setValue(value);
	}

	public void setLeft(Node n) {
		left_child = n;
	}

	public void setRight(Node n) {
		right_child = n;
	}

	public void setParentNode(Node n) {
		parent_node = n;
	}

	public void setValue(int v) {
		value = v;
		getChildren().remove(t);
		t = new Text("" + value);
		t.setX(xVal - 7);
		t.setY(yVal + 5);
		getChildren().add(t);
	}

	public int getrightHeight() {
		return rightHeight;
	}

	public int getleftHeight() {
		return leftHeight;
	}

	public void updateHeight() {

		if (right_child == null)
			rightHeight = 0;

		else {
			if (right_child.rightHeight > right_child.leftHeight) {
				updateRHeight(1 + right_child.rightHeight);
			} else {
				updateRHeight(1 + right_child.leftHeight);

			}
		}

		if (left_child == null) {
			leftHeight = 0;
		} else {
			if (left_child.rightHeight > left_child.leftHeight) {
				updateLHeight(1 + left_child.rightHeight);
			}

			else {
				updateLHeight(1 + left_child.leftHeight);
			}
		}

		if (getParentNode() != null) {
			getParentNode().updateHeight();
		}
	}

	private void updateRHeight(int n) {
		rightHeight = n;
	}

	private void updateLHeight(int n) {
		leftHeight = n;
	}

	public void updateConnectors() {
		
		con = null;
		if(this.getParentNode() != null) {
			con = new connector(this.getParentNode(), this);
			getChildren().add(con);
		}
		
	}

	public void removeConnector() {
		getChildren().remove(con);
	}

}
